# Written By Jubin Mitra
# Dated: 17 - 02 - 2016

set master_service_path [lindex [get_service_paths master] 0]
open_service master $master_service_path
master_read_32 $master_service_path 0x10 0x2

    proc hex2dec {largeHex} {
        set res 0
        foreach hexDigit [split $largeHex {}] {
            set new 0x$hexDigit
            set res [expr {16*$res + $new}]
        }
        return $res
    }

set c "y"

while {$c != "n"} {

    set freq [lindex [master_read_32 $master_service_path 0x28 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set clk_ref_125 [hex2dec $freq_num]
    puts "Frequency clk_ref_125 = [expr $clk_ref_125/10000.00] MHz"

    set freq [lindex [master_read_32 $master_service_path 0x20 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set refclk_sfp_p [hex2dec $freq_num]
    puts "Frequency refclk_sfp_p = [expr $refclk_sfp_p/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x18 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set refclk_qsfp_p [hex2dec $freq_num]
    puts "Frequency refclk_qsfp_p = [expr $refclk_qsfp_p/10000.00] MHz"

    set freq [lindex [master_read_32 $master_service_path 0x10 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set pcie_edge_ob_p_freq [hex2dec $freq_num]
    puts "Frequency pcie_ob_refclk_p = [expr $pcie_edge_ob_p_freq/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x08 0x2] 0]
    set freq_num [format "%0x" $freq]
    set pcie_edge_refclk_p_freq [hex2dec $freq_num]
    puts "Frequency pcie_edge_refclk_p = [expr $pcie_edge_refclk_p_freq/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x00 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set refclk_sma_p [hex2dec $freq_num]
    puts "Frequency refclk_sma_p = [expr $refclk_sma_p/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x38 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set refclk1 [hex2dec $freq_num]
    puts "Frequency refclk1 = [expr $refclk1/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x40 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set refclk4 [hex2dec $freq_num]
    puts "Frequency refclk4 = [expr $refclk4/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x98 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set clk_fpga_b2 [hex2dec $freq_num]
    puts "Frequency clk_fpga_b2 = [expr $clk_fpga_b2/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x90 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set clk_fpga_b3 [hex2dec $freq_num]
    puts "Frequency clk_fpga_b3 = [expr $clk_fpga_b3/10000.00]  MHz"


    set freq [lindex [master_read_32 $master_service_path 0x58 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set refclk_fmca [hex2dec $freq_num]
    puts "Frequency refclk_fmca = [expr $refclk_fmca/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x50 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set refclk_fmcb [hex2dec $freq_num]
    puts "Frequency refclk_fmcb = [expr $refclk_fmcb/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x48 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set refclk_dp [hex2dec $freq_num]
    puts "Frequency refclk_dp = [expr $refclk_dp/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x60 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set clk_emi [hex2dec $freq_num]
    puts "Frequency clk_emi = [expr $clk_emi/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x68 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set refclk_sdi [hex2dec $freq_num]
    puts "Frequency refclk_sdi = [expr $refclk_sdi/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x70 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set fmca_gbtclk_m2c_0 [hex2dec $freq_num]
    puts "Frequency fmca_gbtclk_m2c_0 = [expr $fmca_gbtclk_m2c_0/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x78 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set fmca_gbtclk_m2c_1 [hex2dec $freq_num]
    puts "Frequency fmca_gbtclk_m2c_1 = [expr $fmca_gbtclk_m2c_1/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x80 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set fmcb_gbtclk_m2c_0 [hex2dec $freq_num]
    puts "Frequency fmcb_gbtclk_m2c_0 = [expr $fmcb_gbtclk_m2c_0/10000.00]  MHz"

    set freq [lindex [master_read_32 $master_service_path 0x88 0x2] 0]
    set freq_num [format "%0x" $freq]
    hex2dec $freq_num
    set fmcb_gbtclk_m2c_1 [hex2dec $freq_num]
    puts "Frequency fmcb_gbtclk_m2c_1 = [expr $fmcb_gbtclk_m2c_1/10000.00]  MHz"

    puts -nonewline "\n Do You Want to Repeat (y/n) ? " 
    flush stdout
    set c [gets stdin]
    puts "--------------------------------------------------------------------"
}