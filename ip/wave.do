onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/RESET_I
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/CLK_REF_I
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/CLK_UNKNOWN_I
add wave -noupdate -radix unsigned /frequencycalculator_tb/FrequencyCalculator_comp/CLK_FREQUENCY_VALUE_O
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/COMPUTATION_DONE_O
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/s_REFERENCE_FREQUENCY_VALUE
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/ref_clk_counter
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/unknown_clk_counter
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/ratio_divisor
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/ratio_precision
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/ratio_precision_decimal_places
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/ratio_multiplier
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/ratio_trace_counter
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/unknown_freq_value
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/start_division
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/reset
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/soft_reset
add wave -noupdate -radix hexadecimal /frequencycalculator_tb/FrequencyCalculator_comp/computation_done
add wave -noupdate /frequencycalculator_tb/FrequencyCalculator_comp/unknown_clk_counter_stop
add wave -noupdate /frequencycalculator_tb/toggle_clock
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {21133382637 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 228
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {91223323354 ps}
