-------------------------------------------------------------------------------
-- Title      : Frequency Calculator Qsys Wrapper
-- Project    : Calculation of unknown frequency with respect to known frequency 
-------------------------------------------------------------------------------
-- File       : FrequencyCalculatorQsysWrapper.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 16-02-2015
-- Last update: 16-02-2015
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- General : It has an accuracy of 2 digit after decimal, however it displays upto 4 decimal places. (Value Format : XXX.XX00)
-- Following parameter must be set:
-- Generic parameter: REFERENCE_FREQUENCY_VALUE in integer (MHz)


-- GENERIC : Reference Frequency Value
-- INPUT   : Reference Clock
--			 Unknown   Clock
-- OUTPUT  : Calculated Frequency Value								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 16-02-2015  1.0      Jubin	Created
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;
use IEEE.math_real.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity FrequencyCalculatorQsysWrapper is
   generic (
		constant REFERENCE_FREQUENCY_VALUE    			: integer:= 100
   );
   port (
  
      --================--
      -- Reset & Clocks --
      --================--    
      
      -- Reset:
      ---------
      
      RESET_I                                    : in  std_logic;
  
      -- REFERENCE Clock:
      ---------------
      
      CLK_REF_I   		                         : in  std_logic; 
      
	  -- UNKNOWN Clock:
      ---------------
      
      CLK_UNKNOWN_I   		                     : in  std_logic; 
	  
      --======================--
      --   AVALON MM Slave    --
      --======================-- 
		csi_monitor_clk_clk                                            : in std_logic;
		avs_monitor_read                                               : in std_logic;
		avs_monitor_readdata                                           : out std_logic_vector(31 downto 0);
		avs_monitor_address                                            : in  std_logic_vector( 0 downto 0);
        avs_monitor_write                                              : in std_logic;
		avs_monitor_writedata                                          : in std_logic_vector(31 downto 0)
   
   );
end entity;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--
architecture behavioral of FrequencyCalculatorQsysWrapper is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   
	signal CLK_FREQUENCY_VALUE                                  : std_logic_vector(31 downto 0);
	signal COMPUTATION_DONE                                     : std_logic;     
	signal reset                                                : std_logic:='0';     
	signal s_reset                                              : std_logic:='0';     
    
    --=================================================================================================--
                            --========####  Component Declaration  ####========-- 
    --=================================================================================================--   

    component FrequencyCalculator is
       generic (
            constant REFERENCE_FREQUENCY_VALUE       : integer:= 8
       );
       port (
          RESET_I                                    : in  std_logic;
          CLK_REF_I		                             : in  std_logic; 
          CLK_UNKNOWN_I	                             : in  std_logic; 
          CLK_FREQUENCY_VALUE_O 					 : out std_logic_vector(31 downto 0);
          COMPUTATION_DONE_O                         : out std_logic
       );
    end component;

   
   --==============================================================================================--  

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

   --==================================== User Logic ==============================================--   

   --==================================== Port Mapping ============================================--

   FrequencyCalculator_comp: 
   FrequencyCalculator
      generic map(
        REFERENCE_FREQUENCY_VALUE               => REFERENCE_FREQUENCY_VALUE
      )
      port map(
      RESET_I                                   => reset,
      CLK_REF_I   	                            => CLK_REF_I,
      CLK_UNKNOWN_I   	                        => CLK_UNKNOWN_I,
      CLK_FREQUENCY_VALUE_O                     => CLK_FREQUENCY_VALUE,
      COMPUTATION_DONE_O                        => COMPUTATION_DONE
   );   
   
        reset <= RESET_I or s_reset;
    
	--================================== Avalon Slave =============================================--
    
    --==============================--
		--   MONITOR AVALON MM Slave    --
		--==============================-- 
		
		----------
		-- READ --
		----------
		process(RESET_I,csi_monitor_clk_clk,avs_monitor_read) 
		   begin
			if RESET_I = '1' then
				avs_monitor_readdata <= (others=>'0');
			else
				if rising_edge(csi_monitor_clk_clk) then
					if avs_monitor_read='1' then
						case avs_monitor_address is
                            when "0" => avs_monitor_readdata <=  CLK_FREQUENCY_VALUE;
                            when "1" => avs_monitor_readdata <=  x"0000_000" & "000" & COMPUTATION_DONE;
                            when others => avs_monitor_readdata <= (others=>'0');
                        end case;
                    end if;
                end if;
            end if;
        end process;
        
        -----------
        -- WRITE --
        -----------
        process(RESET_I,csi_monitor_clk_clk,avs_monitor_write) 
		   begin
			if RESET_I = '1' then
				s_reset			                               <='0';
			else
				if rising_edge(csi_monitor_clk_clk) then
					if avs_monitor_write='1' then
						case avs_monitor_address is
							-- LINK 1
							when "0" 	=> s_reset 		<= avs_monitor_writedata(0);
                            when others => s_reset 		<= s_reset;
                        end case;
                    end if;
                end if;
            end if;
        end process;
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--