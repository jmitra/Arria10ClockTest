-------------------------------------------------------------------------------
-- Title      : Arria 10 Clock Frequency Monitor
-- Project    : See for input   pcie_ob_refclk_p, //LVDS On-board programmable Ref Clock, Default 100MHz
-- input   pcie_edge_refclk_p,     //HCSL    //PCIe Clock- Terminate on MB
-- input   clk_50,   //1.8V - 50MHz
-- input   clk_125_p,  //LVDS - 125MHz
-- input   refclk_sfp_p,   //LVDS - 644.53125MHz default, programmable
-- input   refclk_qsfp_p,    //LVDS - 644.53125MHz default, programmable 
-------------------------------------------------------------------------------
-- File       : top.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 16-02-2015
-- Last update: 16-02-2015
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- TOP FILE for running the code in Arria 10 (	10AX115S3F45E2SGE3 )						
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 17-02-2015  1.0      Jubin	Created
-------------------------------------------------------------------------------

--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;


--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity top is
   port (
      clk_125_p  		                        : in  std_logic; 
      clk_50   		                        : in  std_logic; 
      pcie_edge_refclk_p                     : in  std_logic; 
      pcie_ob_refclk_p                       : in  std_logic; 
      refclk_qsfp_p                          : in  std_logic; 
      refclk_sfp_p                           : in  std_logic;
		refclk_sma_p       						   : in  std_logic; 	
		clk_emi_p	       						   : in  std_logic; 	
		refclk1_p	       						   : in  std_logic; 	
		refclk4_p	       						   : in  std_logic; 	
		refclk_dp_p	       						   : in  std_logic; 	
		refclk_fmca_p      						   : in  std_logic; 	
		refclk_fmcb_p      						   : in  std_logic; 	
		refclk_sdi_p      						   : in  std_logic; 	
		fmca_gbtclk_m2c_p						   	: in  std_logic_vector(1 downto 0); 	
		fmcb_gbtclk_m2c_p						      : in  std_logic_vector(1 downto 0);
		clk_fpga_b3_p									: in  std_logic;
		clk_fpga_b2_p									: in  std_logic
   );
end entity;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--
architecture behavioral of top is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   

	
	
	--=================================================================================================--
							--========####  Component Declaration  ####========-- 
	--=================================================================================================--   

    component clks_freq_monitor is
        port (
            clk_125_clk             : in std_logic := 'X'; -- clk
            clk_50_clk              : in std_logic := 'X'; -- clk
            clk_emi_p_clk           : in std_logic := 'X'; -- clk
            fmca_gbtclk_m2c_0_p_clk   : in std_logic := 'X'; -- clk
            pcie_edge_refclk_p_clk  : in std_logic := 'X'; -- clk
            pcie_ob_refclk_p_clk    : in std_logic := 'X'; -- clk
            refclk1_p_clk           : in std_logic := 'X'; -- clk
            refclk4_p_clk           : in std_logic := 'X'; -- clk
            refclk_dp_p_clk         : in std_logic := 'X'; -- clk
            refclk_fmca_p_clk       : in std_logic := 'X'; -- clk
            refclk_fmcb_p_clk       : in std_logic := 'X'; -- clk
            refclk_qsfp_p_clk       : in std_logic := 'X'; -- clk
            refclk_sdi_p_clk        : in std_logic := 'X'; -- clk
            refclk_sfp_p_clk        : in std_logic := 'X'; -- clk
            refclk_sma_p_clk        : in std_logic := 'X'; -- clk
            reset_reset_n           : in std_logic := 'X'; -- reset_n
            fmca_gbtclk_m2c_1_p_clk : in std_logic := 'X'; -- clk
            fmcb_gbtclk_m2c_0_p_clk : in std_logic := 'X'; -- clk
            fmcb_gbtclk_m2c_1_p_clk : in std_logic := 'X'; -- clk
				clk_fpga_b3_p_clk       : in std_logic := 'X'; -- clk
            clk_fpga_b2_p_clk       : in std_logic := 'X'  -- clk
        );
    end component clks_freq_monitor;
   --==============================================================================================--  

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

   --==================================== User Logic ==============================================--   

    u0 : component clks_freq_monitor
        port map (
            clk_125_clk             => clk_125_p,
            clk_50_clk              => clk_50, 
            clk_emi_p_clk           => clk_emi_p,    
            fmca_gbtclk_m2c_0_p_clk => fmca_gbtclk_m2c_p(0),
            pcie_edge_refclk_p_clk  => pcie_edge_refclk_p,
            pcie_ob_refclk_p_clk    => pcie_ob_refclk_p,  
            refclk1_p_clk           => refclk1_p,       
            refclk4_p_clk           => refclk4_p,
            refclk_dp_p_clk         => refclk_dp_p,     
            refclk_fmca_p_clk       => refclk_fmca_p,  
            refclk_fmcb_p_clk       => refclk_fmcb_p,
            refclk_qsfp_p_clk       => refclk_qsfp_p,
            refclk_sdi_p_clk        => refclk_sdi_p,
            refclk_sfp_p_clk        => refclk_sfp_p,       
            refclk_sma_p_clk        => refclk_sma_p,  
            reset_reset_n           => '1',
            fmca_gbtclk_m2c_1_p_clk => fmca_gbtclk_m2c_p(1),
            fmcb_gbtclk_m2c_0_p_clk => fmcb_gbtclk_m2c_p(0),
            fmcb_gbtclk_m2c_1_p_clk => fmcb_gbtclk_m2c_p(1),
				clk_fpga_b3_p_clk       => clk_fpga_b3_p,
            clk_fpga_b2_p_clk       => clk_fpga_b2_p
        );

    
	
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--