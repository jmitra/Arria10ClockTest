quartus_map="/opt/altera/15.1/quartus/bin/quartus_map"
quartus_fit="/opt/altera/15.1/quartus/bin/quartus_fit"
quartus_asm="/opt/altera/15.1/quartus/bin/quartus_asm"
quartus_sta="/opt/altera/15.1/quartus/bin/quartus_sta"

qpf_file="Arria10ClockTest"
qsf_file="Arria10ClockTest"

time $quartus_map --read_settings_files=on --write_settings_files=off $qpf_file -c $qsf_file 
time $quartus_fit --read_settings_files=on --write_settings_files=off $qpf_file -c $qsf_file 
time $quartus_asm --read_settings_files=on --write_settings_files=off $qpf_file -c $qsf_file 
time $quartus_sta $qpf_file -c $qsf_file

