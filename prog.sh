quartus_pgm="/opt/altera/15.1/quartus/bin/quartus_pgm"
jtagconfig="/opt/altera/15.1/quartus/bin/jtagconfig"
system_console="/opt/altera/15.1/quartus/sopc_builder/bin/system-console"

qpf_file="Arria10ClockTest"
qsf_file="Arria10ClockTest"

echo Adding Jtag Server
$jtagconfig --addserver crorcdev a10gx
echo Enumerating Jtag Links
$jtagconfig --enum
time $quartus_pgm  -m JTAG -o "\"p\;./output_files/"$qsf_file.sof\"
time $system_console --script=read_registers_system_console.tcl
